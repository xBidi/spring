package com.template.spring.controller;

import com.template.spring.model.PingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class PingController {

    @GetMapping(path = "/ping")
    public PingResponse pingGet() {
        return new PingResponse("ok");
    }
}


