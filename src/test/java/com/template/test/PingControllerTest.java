package com.template.test;

import com.template.spring.controller.PingController;
import com.template.spring.model.PingResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PingController.class)
@Slf4j
public class PingControllerTest {

    @Autowired
    private PingController pingController;

    @Test
    public void pingGet() {
        System.out.println("Starting test");
        // -- TESTING
        PingResponse pingResponse = this.pingController.pingGet();
        String result = pingResponse.toString();
        String expectedResult = "PingResponse(message=ok)";
        // -- END TESTING
        System.out.println(String.format("Result -> %s", result));
        System.out.println(String.format("ExpectedResult -> %s", expectedResult));
        System.out.println("Checking function -> Assert.assertEquals()");
        Assert.assertEquals(pingResponse.getMessage(), "ok");
        System.out.println("Ending test");
    }
}