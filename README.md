# spring
Project template for java spring

code style
````
https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml
````
package manager
````
maven
````
java jdk
````
openjdk-8
````

generate jar
````
mvn install
````

launch server
````
java -jar target/spring-0.0.0.0.jar
````

launch server docker
````
docker image build -t spring .
docker stop spring
docker rm spring
docker run -p 8081:8080 --name spring -d spring
docker logs -f spring

````

default swagger url
````
http://localhost:8081/swagger-ui.html
````

default working url
````
http://localhost:8081/ping
````
deploy on heroku
````
heroku login
heroku create
git push heroku master
heroku ps:scale web=1
heroku open
heroku logs --tail
````
