#!/usr/bin/env bash
docker image build -t spring .
docker stop spring
docker rm spring
docker run -p 8081:8080 --name spring -d spring
